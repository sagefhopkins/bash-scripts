#!/bin/bash

function read_files_in_dir {
    for file in *; do
        if [ -f "$file" ]; then
            file_info=$(ls -lt "$file")  # Get the file information sorted by modification time
            last_modified=$(echo "$file_info" | awk '{print $6, $7, $8}')  # Extract the date and time of last modification
            echo "$(head -n 1 "$file")  Last Modified: $last_modified"
            echo "File: $file"
        fi
    done
}

for folder in */; do
    folder_name=$(basename "$folder")
    echo "Processing folder: $folder_name"
    cd "$folder_name"
    read_files_in_dir
    cd ..
done